package com.lut.student.taipale.harjoitustyo;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    ListView myListView;
    String[] breeds;
    String[] descriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources res = getResources();
        myListView = (ListView) findViewById(R.id.myListView);
        breeds = res.getStringArray(R.array.breeds);
        descriptions = res.getStringArray(R.array.descriptions);

        BreedAdapter BreedAdapter = new BreedAdapter(this, breeds, descriptions);
        myListView.setAdapter(BreedAdapter);

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent showDetailActivity = new Intent(getApplicationContext(), DetailActivity.class);
                showDetailActivity.putExtra("lut.student.taipale.INDEX", i);
                startActivity(showDetailActivity);
            }
        });

    }
}
