package com.lut.student.taipale.harjoitustyo;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {

    String[] address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        Intent in = getIntent();
        int index = in.getIntExtra("lut.student.taipale.INDEX", -1);

        if (index > -1) {
            int pic = getImg(index);
            ImageView img = (ImageView) findViewById(R.id.imageView);
            scaleImg(img, pic);
            getAddress(index);
        }
    }

    private int getImg(int index) {
        switch (index) {
            case 0:
                return R.drawable.labrador;
            case 1:
                return R.drawable.poodle;
            case 2:
                return R.drawable.bordercollie;
            case 3:
                return R.drawable.husky;
            default:
                return -1;
        }
    }

    private void scaleImg(ImageView img, int pic) {
        Display screen = getWindowManager().getDefaultDisplay();
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), pic, options);

        int imgWidth = options.outWidth;
        int screenWidth = screen.getWidth();

        if (imgWidth > screenWidth) {
            int ratio = Math.round((float) imgWidth / (float) screenWidth);
            options.inSampleSize = ratio;
        }

        options.inJustDecodeBounds = false;
        Bitmap scaledImg = BitmapFactory.decodeResource(getResources(), pic, options);
        img.setImageBitmap(scaledImg);

    }

    private void getAddress(final int index) {
        Resources res = getResources();
        address = res.getStringArray(R.array.address);

        Button webBtn = (Button) findViewById(R.id.webBtn);
        webBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view){
                String wpage = address[index];
                Uri webaddress = Uri.parse(wpage);


                Intent gotoWpage = new Intent(Intent.ACTION_VIEW, webaddress);
                if (gotoWpage.resolveActivity(getPackageManager()) != null) {
                    startActivity(gotoWpage);
                }
            }
        });
    }
}
