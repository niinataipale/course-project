package com.lut.student.taipale.harjoitustyo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class BreedAdapter extends BaseAdapter {

    LayoutInflater mInflater;
    ListView myListView;
    String[] breeds;
    String[] descriptions;

    public BreedAdapter(Context c, String[] i, String[] p) {
        breeds = i;
        descriptions = p;
        mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return breeds.length;
    }

    @Override
    public Object getItem(int i) {
        return breeds[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = mInflater.inflate(R.layout.my_listview_detail, null);
        TextView breedTextView = (TextView) v.findViewById(R.id.breedTextView);
        TextView descriptionTextView = (TextView) v.findViewById(R.id.descriptionTextView);

        String brd = breeds[i];
        String desc = descriptions[i];

        breedTextView.setText(brd);
        descriptionTextView.setText(desc);

        return v;
    }
}
